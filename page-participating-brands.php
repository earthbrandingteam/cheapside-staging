<?php  
/*
Template Name: Participating Brands
*/
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
			<div id="mainPage">
				<?php get_sidebar(); ?>				
				<div id="mainContent">
					<div id="pageTitle" class="halfheight">
						<h1 class="blue">Participating Brands</h1>
					</div>
                    
                    
                    
					<div id="partBrandsContent">
                    
                    <?php the_content(); ?>
                    
                    
					<?php 
						query_posts( array ( 
						'post_type' => 'participating-brands', 
						'showposts' => -1, 
						'post_status' => 'publish', 
						'orderby' => 'menu_order',
						'order' => 'ASC'
						) );
						if ( have_posts() ) : while ( have_posts() ) : the_post();
					?>

						<div class="contentBox">
							<div class="imageBox">
								<?php 
								if ( has_post_thumbnail($post->ID))
								{
									echo get_the_post_thumbnail($post->ID, '115x115');
								}
								?>
							</div>
							<div class="textBox">
								<div class="blue" style="text-align: center;"><?php the_title(); ?></div>
							</div>
						</div>
						<?php endwhile; ?>
						<?php endif; ?>
        				<?php wp_reset_query(); ?>
					</div>
					<div class="clearFloat"></div>
					
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>