<?php  
/*
Template Name: Home Page
*/
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
			<div id="mainPage">
				<?php /* get_sidebar(); */ ?>				
				<div id="mainContent">
					<!-- <div id="pageTitle">
						<h1 class="blue">Cheapside Privilege Card</h1>
					</div> -->
					<div id="homePageImage">
						<?php echo do_shortcode("[metaslider id=912]"); ?>					
					</div>
					<div id="mainContentBody">

						<div id="homePageText" class="left">
							<div class="text black"><?php echo the_content() ?></div>
						</div>
						
						<div id="homePageMenu" class="left">
							<?php wp_nav_menu(array('menu' => 'Page Navigation Menu', 'menu_class' => 'menu-page-navigation-menu')); ?>
						</div>
						
					</div>  <!-- END MAINCONTENTBODY-->
					<div class="clearFloat"></div>
			
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
			<div class="clearFloat"></div>
<?php 
endwhile; endif;
get_footer(); ?>