	<div id="footer" class="clearfix">
		<div id="footerText">
			<p>&copy; <?php echo date("Y"); ?> Cheapside Business Alliance &#47; content management by incontrolCMS.net</p>
			<div id="footerlineleft"><a href="/privacy-and-accessibility/">Privacy and Accessibility</a> - <a href="/terms-and-conditions/">Terms and Conditions</a></div>
			<div id="footerlineright">Website designed and developed by <a href="https://earth.london/" target="_blank">Earth</a></div>
		</div>
	</div>
	
</div>

<?php wp_footer(); ?>

<script type="text/javascript">
<!--
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38785903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
-->
</script>
    
    
<script type="text/javascript">
<!--

jQuery(document).ready(function()
{
	jQuery('.menu-link').bigSlide();
});		

jQuery(window).load(function()
{
	jQuery(function() {
		jQuery('a[href$=".pdf"]').attr('target', '_blank');
		jQuery('a[href$=".zip"]').attr('target','_blank');
		jQuery('a[href$=".rar"]').attr('target','_blank');
		jQuery('a[href$=".txt"]').attr('target','_blank');
		jQuery('a[href$=".doc"]').attr('target','_blank');
		jQuery('a[href$=".docx"]').attr('target','_blank');
		jQuery('a[href$=".xls"]').attr('target','_blank');
		jQuery('a[href$=".xlsx"]').attr('target','_blank');
		jQuery("a[href^='http:']").not("[href*='"+ location.hostname +"']").attr('target','_blank');
		jQuery("a[href^='https:']").not("[href*='"+ location.hostname +"']").attr('target','_blank');
	});
});

jQuery(window).resize(function()
{
	if(jQuery(document).width() > 991)
	{
		if(jQuery("#mobile-menu").hasClass("active"))
		{
			jQuery("#mobile-menu").trigger("click");
		}
	}
});

-->
</script>
</body>
</html>