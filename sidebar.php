<div id="sideBar">

	<?php wp_nav_menu(array('menu' => 'Main Navigation Menu', 'menu_class' => 'menu-offers-navigation-menu')); ?>

	<?php 
	if(!is_front_page())
	{
		echo '<hr />';
		wp_nav_menu(array('menu' => 'Page Navigation Menu', 'menu_class' => 'menu-page-navigation-menu'));
		echo '<hr />';
	}
	?>

	<!-- <div class="shareThis socialMediaSharing">
		<p class="share-this-headline"><strong>Share this page</strong></p>
		<?php /* get_template_part('share-this'); */ ?>
	</div> -->

</div>