<?php  
/*
Template Name: Privilege Card Order
*/
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
				<div id="mainPage">
				<?php get_sidebar(); ?>				
				<div id="mainContent">
					<div id="pageTitle" class="halfheight">
						<h1 class="blue">Order Your Card</h1>
					</div>
                    
                    <div id="privacyAndAccessibilityContent">
						<div class="text black"><?php the_content(); ?></div>
					</div>
                    
					<div id="cardOrderForm">
                    
					
                    
						<div id="card-activation-form">
							
                            
<script type="text/javascript">
<!--
var RecaptchaOptions = {
	theme : 'clean',
	custom_theme_widget: 'recaptcha_widget'
};          
-->
</script>               
                            
                            
                            
<?php 

$success = 0;

$recaptcha=$_POST['g-recaptcha-response'];
if(!empty($recaptcha))
{
		
	if(isSet($_POST['add_order']))
	{
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret='6LcSd08UAAAAAEsIkx9MtZoMLh-Xi3QQj98jKN-u';
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
		$res=getCurlData($url);
		$res= json_decode($res, true);
		if(
			$res['success'] &&
			$_POST['_firstname'] != "" &&
			$_POST['_lastname'] != "" &&
			valid_email(clean_data_escape_string($_POST['_email'])) &&
			$_POST['_contact_number'] != "" &&
			$_POST['_address_line_1'] != "" &&
			$_POST['_town'] != "" &&
			$_POST['_postcode'] != "" &&
			$_POST['_terms'] == "1"
			) 
		 {
	  
		mysqli_query($db_link, "INSERT INTO cms_orders VALUES(
			NULL,
			'1',
			'".date("Y-m-d H:i:s")."',
			'".clean_data_escape_string($_POST['_firstname'])."',
			'".clean_data_escape_string($_POST['_lastname'])."',
			'".clean_data_escape_string($_POST['_email'])."',
			'".clean_data_escape_string($_POST['_contact_number'])."',
			'".clean_data_escape_string($_POST['_company_name'])."',
			'".clean_data_escape_string($_POST['_address_line_1'])."',
			'".clean_data_escape_string($_POST['_address_line_2'])."',
			'".clean_data_escape_string($_POST['_address_line_3'])."',
			'".clean_data_escape_string($_POST['_town'])."',
			'".clean_data_escape_string($_POST['_postcode'])."',
			'".mysqli_real_escape_string($db_link, $_POST['_message'])."',
			'".clean_data_escape_string($_POST['_accept_newsletters'])."',
			'".clean_data_escape_string($_POST['_terms'])."'
		)");
	
		$activation_id = mysqli_insert_id($db_link);
	
		$name_notify = clean_data_escape_string($_POST['_firstname']) . ' ' . clean_data_escape_string($_POST['_lastname']) . ' (' . clean_data_escape_string($_POST['_contact_number']) . ') ';
	
		send_confirmation('request', clean_data_escape_string($_POST['_email']),clean_data_escape_string($_POST['_firstname']));
		send_confirmation('notify-admin-member',"info@incheapside.com", $name_notify);
		send_confirmation('notify-admin-member',"Claire.Dumontier-Marriage@cityoflondon.gov.uk", $name_notify);
	
		$success = 1;
	
		echo '<h2><strong>Your details have been submitted succesfully.</strong></h2>';
	
		}
	}

}

if($success != 1)
{
?>
 
<p class="smaller-font-form"><strong><em>* Required Fields. To deliver your card successfully, we require a company name and full address.</em></strong></p>

<hr />
 
      <form name="manage_content" action="/order-a-card/" method="post" enctype="multipart/form-data" id="custom-forms">
        <input type="hidden" name="add_order" value="go" />
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
   
          <?php if(($_POST['add_order'] != "") && ($_POST['_firstname'] == "")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter your First Name</span></td>
          </tr>
          <?php } ?>
          
          <tr>
            <td height="25" valign="bottom"><label for="_firstname">First Name *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_firstname" name="_firstname" class="input_box" value="<?= $_POST['_firstname']; ?>" /></td>
          </tr>
          
          <?php if(($_POST['add_order'] != "") && ($_POST['_lastname'] == "")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter your Last Name</span></td>
          </tr>
          <?php } ?>
          
          <tr>
            <td height="25" valign="bottom"><label for="_lastname">Last Name *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_lastname" name="_lastname" class="input_box" value="<?= $_POST['_lastname']; ?>" /></td>
          </tr>
          
          <?php if(($_POST['add_order'] != "") && (valid_email(clean_data_escape_string($_POST['_email'])) == false)){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter a valid email address</span></td>
          </tr>
          <?php } ?>
          
          <tr>
            <td height="25" valign="bottom"><label for="_email">Email Address *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_email" name="_email" class="input_box" value="<?= $_POST['_email']; ?>" /></td>
          </tr>
          
          
           <?php if(($_POST['add_order'] != "") && ($_POST['_contact_number'] == "")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter your contact number</span></td>
          </tr>
          <?php } ?>
          
          
          <tr>
            <td height="25" valign="bottom"><label for="_contact_number">Contact Number *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_contact_number" name="_contact_number" class="input_box" value="<?= $_POST['_contact_number']; ?>" /></td>
          </tr>
          
          
          <tr>
            <td height="25" valign="bottom"><label for="_contact_number">Company Name</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_company_name" name="_company_name" class="input_box" value="<?= $_POST['_company_name']; ?>" /></td>
          </tr>
          
          
           <?php if(($_POST['add_order'] != "") && ($_POST['_address_line_1'] == "")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter your Address</span></td>
          </tr>
          <?php } ?>
          
          
          <tr>
            <td height="25" valign="bottom"><label for="_address_line_1">Address Line 1 *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_address_line_1" name="_address_line_1" class="input_box" value="<?= $_POST['_address_line_1']; ?>" /></td>
          </tr>
           
           
          
          <tr>
            <td height="25" valign="bottom"><label for="_address_line_2">Address Line 3</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_address_line_2" name="_address_line_2" class="input_box" value="<?= $_POST['_address_line_2']; ?>" /></td>
          </tr>
          
           <tr>
            <td height="25" valign="bottom"><label for="_address_line_3">Address Line 3</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_address_line_3" name="_address_line_3" class="input_box" value="<?= $_POST['_address_line_3']; ?>" /></td>
          </tr>
          
          <tr>
            <td height="25" valign="bottom"><label for="_town">Town</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_town" name="_town" class="input_box" value="<?= $_POST['_town']; ?>" /></td>
          </tr>
          
          
           <?php if(($_POST['add_order'] != "") && ($_POST['_postcode'] == "")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter your Postcode</span></td>
          </tr>
          <?php } ?>
          
           <tr>
            <td height="25" valign="bottom"><label for="_postcode">Postcode *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_postcode" name="_postcode" class="input_box" value="<?= $_POST['_postcode']; ?>" /></td>
          </tr>
          
          <tr>
            <td height="25" valign="bottom"><label for="_message">Message</label></td>
          </tr>
          <tr>
            <td valign="top"><textarea id="_message" name="_message" class="input_box"><?= $_POST['_message']; ?></textarea></td>
          </tr>
          
          
          
          
          
          
          
          <tr>
            <td height="25" valign="bottom"><label for="_accept_newsletters">Contact Permission</label></td>
          </tr>
          <tr>
            <td valign="top">
            
            Please click this opt button to receive your card with offers &amp; updates by email.<br />
            <a href="/privacy-and-accessibility/">Click here to read our Privacy terms</a>.
            <br /><br />
            <label><input type="checkbox" id="_accept_newsletters" name="_accept_newsletters" class="input_box" value="1"> Yes please, sign me up to the Cheapside Privilege Card.</label></td>
          </tr>
        
          <tr>
            <td height="25" valign="bottom">&nbsp;</td>
          </tr>
          
          <tr>
            <td height="25" valign="bottom"><label for="_terms">Terms &amp; Conditions</label></td>
          </tr>
          <tr>
            <td valign="top"><input type="checkbox" id="_terms" name="_terms" class="input_box" value="1"> Please tick this box to confirm you agree to the <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></td>
          </tr>
          
          <?php if(($_POST['add_order'] != "") && ($_POST['_terms'] != "1")){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please agree to the Terms &amp; Conditions</span></td>
          </tr>
          <?php } ?>
          
          
          
           <tr>
            <td height="30">&nbsp;</td>
          </tr>
          
          
          
          <tr>
            <td>
            
            <?php 
			if(isSet($_POST['add_order']))
			{
				if(!$res['success'])
				{ 
					echo '<p><span class="error">Please solve the puzzle below</span></p>'; 
				}
			}	
			else
			{
				echo '<p><label for="recaptcha_response_field">Please solve the puzzle below</label></p>';
			} 
			?>
            
			<div id="captcha-container">
            
              <div class="g-recaptcha" data-sitekey="6LcSd08UAAAAAHjcpDJYPEjHGaolgogbVJ8uxcDc"></div>
            
            </div>
            </td>
          </tr>
          
          
          
          
          
           <tr>
            <td height="30">&nbsp;</td>
          </tr>
          
          <tr>
            <td valign="bottom"><input type="submit" name="submit" value="Order Your Card" /></td>
          </tr>
          
        </table>
      </form>
<?php
}
?>
                            
                            
                            
                            
                            
                            
                            
                            
						</div>
					</div>
					<div class="clearFloat"></div>
					
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>