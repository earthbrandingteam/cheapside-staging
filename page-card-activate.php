<?php  
/*
Template Name: Privilege Card Activation
*/
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
<div id="mainPage">
	<?php get_sidebar(); ?>				
	<div id="mainContent">
		<div id="pageTitle" class="halfheight">
			<h1 class="blue">Activate Your Card</h1>
		</div>
		
		<div id="privacyAndAccessibilityContent">
			<div class="text black"><?php the_content(); ?></div>
		</div>
		
		<div id="cardOrderForm">
                    
					
                    
		<div id="card-activation-form">
							
                            
<script type="text/javascript">
<!--
var RecaptchaOptions = {
	theme : 'clean',
	custom_theme_widget: 'recaptcha_widget'
};          
-->
</script>
                 
<?php 

$success = 0;

$recaptcha=$_POST['g-recaptcha-response'];
if(!empty($recaptcha))
{
		
	if(isSet($_POST['add_activations']))
	{
		$google_url="https://www.google.com/recaptcha/api/siteverify";
		$secret='6LcSd08UAAAAAEsIkx9MtZoMLh-Xi3QQj98jKN-u';
		$ip=$_SERVER['REMOTE_ADDR'];
		$url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
		$res=getCurlData($url);
		$res= json_decode($res, true);
		if(
		$res['success'] &&
		valid_email(clean_data_escape_string($_POST['_email']))
		) 
	 	{
			$success = 1;
			echo '<h2><strong>Thank you for activating your Privilege Card.</strong></h2>';
		}
	}
}

if($success != 1)
{
?>
 
<p><strong><em>* Required Fields</em></strong></p>

<hr />
 
      <form name="manage_content" action="/activate-privilege-card/" method="post" enctype="multipart/form-data" id="custom-forms">
        <input type="hidden" name="add_activations" value="go" />
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
   
          <?php if(($_POST['add_activations'] != "") && (valid_email(clean_data_escape_string($_POST['_email'])) == false)){ ?>
            <tr>
            <td height="25" valign="bottom"><span class="error">Please enter a valid email address</span></td>
          </tr>
          <?php } ?>
          
           <tr>
            <td height="25" valign="bottom"><label for="_email">Email Address *</label></td>
          </tr>
          <tr>
            <td height="55"><input type="text" id="_email" name="_email" class="input_box" value="<?= $_POST['_email']; ?>" /></td>
          </tr>
           

          <tr>
            <td>
       
       		
       		<?php 
			if(isSet($_POST['add_activations']))
			{
				if(!$res['success'])
				{ 
					echo '<p><span class="error">Please solve the puzzle below</span></p>'; 
				}
			}	
			else
			{
				echo '<p><label for="recaptcha_response_field">Please solve the puzzle below</label></p>';
			} 
			?>
			
            
			<div id="captcha-container">
            
         
			
			
			 <div class="g-recaptcha" data-sitekey="6LcSd08UAAAAAHjcpDJYPEjHGaolgogbVJ8uxcDc"></div>
			
			
			
            <br />
            <p><strong>Should you encounter any problems in activating your card,<br />
please <a href="/contact-us">contact us</a></strong></p>
            </div>
            </td>
          </tr>
          <tr>
            <td valign="bottom"><input type="submit" name="submit" value="Activate Your Card" /></td>
          </tr>
          
        </table>
      </form>
<?php
}
?>
                            
                            
                            
                            
                            
                            
                            
                            
						</div>
					</div>
					<div class="clearFloat"></div>
					
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>