<?php 
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
<div id="mainPage">
	<?php get_sidebar(); ?>				
	<div id="mainContent">
		<div id="pageTitle">
			<h1 class="blue"><?php the_title(); ?></h1>
		</div>
		
		<?php 
		if ( has_post_thumbnail($post->ID))
		{
			echo get_the_post_thumbnail($post->ID, 'medium', array( 'class' => 'offersSingleImage' ));
		}
		?>
								
		<div class="text black"><?php the_content(); ?></div>
		
		<br />
		<br />
		
		<div class="clearfix">
		
		<div style="width: 50%; float: left;">
			<div style="padding-right: 20px; text-align: left;">
				<?php next_post_link('%link', '<strong>&laquo; Next offer</strong>'); ?>
			</div>
		</div>
		
		<div style="width: 50%; float: right;">
			<div style="padding-left: 20px; text-align: right;">
				<?php previous_post_link('%link', '<strong>Previous offer &raquo;</strong>'); ?>
			</div>
		</div>
		
		</div>
		
		<br />
		<br />
		
	</div> <!-- END MAINCONTENT -->
</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>