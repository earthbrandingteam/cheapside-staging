
<?php
wp_reset_query();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

if($image[0] != "")
{
	$image = $image[0];
}
else
{
	$image = "https://www.cheapsideprivilegecard.co.uk/wp-content/uploads/2012/06/Pcardhome-page2.jpg";
}
wp_reset_query();
?>

<div class="show_mobile">
<span class='st_sharethis_large st_sharethis_icon' displayText='ShareThis' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
</div>

<div class="hide_mobile">
<span class='st_sharethis_large st_sharethis_icon' displayText='ShareThis' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<span class='st_twitter_large st_twitter_icon' displayText='Tweet' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<span class='st_linkedin_large st_linkedin_icon' displayText='LinkedIn' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<div class="clear_mobile"></div>
<span class='st_googleplus_large st_googleplus_icon' displayText='Google +' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<span class='st_pinterest_large st_pinterest_icon' displayText='Pinterest' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<span class='st_email_large st_email_icon' displayText='Email' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
</div>

<div class="hide_mobile">
<span class='st_facebook_large st_facebook_icon' displayText='Facebook' st_image="<?= $image; ?>" st_url="<?php the_permalink(); ?>" st_title="<?= strip_tags(get_the_title()); ?>" st_summary="<?= strip_tags(get_the_excerpt()); ?>"></span>
<span class='st_fblike_large' displayText='Facebook Like'></span>
<span class='st_fbrec_large' displayText='Facebook Recommend'></span>
</div>
