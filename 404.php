<?php 
get_header();
?>
			<div id="mainPage">
				<?php get_sidebar(); ?>				
				<div id="mainContent">
					<div id="pageTitle">
						<h1 class="blue">Page not found</h1>
					</div>
					<div id="privacyAndAccessibilityContent">
						<div class="text black">Back to <a href="/">home page</a>.</div>
					</div>
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
get_footer(); ?>