<?php  
/*
Template Name: Leisure and Fitness
*/
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
			<div id="mainPage">
				<?php /* get_sidebar(); */ ?>				
				<div id="mainContent">
					<?php if (has_post_thumbnail( $post->ID ) ): ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						<img id="custom-bg" src="<?php echo $image[0]; ?>')">
					<?php endif; ?>

					<div class="page-top">
						<div id="pageTitle" class="halfheight">
						<h1 class="blue">Leisure and <br>Fitness</h1>
						</div>

						<div id="homePageMenu" class="left">
							<?php wp_nav_menu(array('menu' => 'Page Navigation Menu', 'menu_class' => 'menu-page-navigation-menu')); ?>
						</div>
					</div>

					<div id="offersContent">
						<div class="pageNavigation">
							
							<?php 
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							query_posts( array ( 	'post_type' => 'leisure-and-fitness', 
													'showposts' => 10, 
													'post_status' => 'publish', 
													'orderby' => 'menu_order',
													'order' => 'ASC',
													'paged' => $paged) );
							// --- Pagination
							if (function_exists('wp_paginate'))
							{
								wp_paginate();
							}
							// ---
							?>
						</div>
						<div class="clearFloat"></div>
						<?php 
							query_posts( array ( 	'post_type' => 'leisure-and-fitness', 
													'showposts' => 10, 
													'post_status' => 'publish',
													'orderby' => 'menu_order',
													'order' => 'ASC',
													'paged' => $paged) );
													
							if ( have_posts() ) : while ( have_posts() ) : the_post();
						?>
						<div class="offers">
							
							<div class="offersText">
								<div class="text black">
								
								
                                <?php 
								if ( has_post_thumbnail($post->ID))
								{
									echo '<a href="'.get_the_permalink().'"><div class="offersThumbnail" style="background-image: url('.getImageURL(get_post_thumbnail_id($post->ID), 'medium').');';
									if(get_field('featured_image_background_size') != 'none')
									{
										echo 'background-size: '.get_field('featured_image_background_size').';';
									}
									echo '"></div></a>';
								}
								?>
                            	
                            	<div class="offersTitle">
								<h3 class="blue"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								</div>
							
								
								<p class="read-more"><strong><a href="<?php the_permalink(); ?>">Read more &raquo;</a></strong></p>
								
								
								</div>
							</div>
						</div>

						<?php endwhile; ?>
                        
                        <div class="pageNavigation" style="margin-top: 30px;">
							<?php 
				
							// --- Pagination
							if (function_exists('wp_paginate'))
							{
								wp_paginate();
							}
							// ---
							?>
						</div>
                        
						<?php endif; ?>
        				<?php wp_reset_query(); ?>
					</div>
					<div class="clearFloat"></div>
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>