<?php
	define('STYLESHEET', get_bloginfo('stylesheet_url'));
	define('IMAGES_DIR', get_bloginfo('template_url').'/images/');
	define('TEMPLATE_DIR', get_bloginfo('template_url').'/');
	define('JS_DIR', get_bloginfo('template_url').'/js/');
	define('BLOGINFO_NAME', get_bloginfo('name'));
	define('DESCRIPTION', get_bloginfo('description'));
	define('BLOGINFO_RSS2', get_bloginfo('rss2_url'));
	define('BLOGINFO_RSS', get_bloginfo('rss_url'));
	define('BLOGINFO_ATOM', get_bloginfo('atom_url')); 
	define('HOME', get_option('home').'/');
	define('PINGBACK', get_bloginfo('pingback_url'));
	
	session_start();
	
	global $db_link;
	$db_link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
	mysqli_select_db($db_link, "cheap_live2013");


	// Add RSS links to <head> section
	automatic_feed_links();
	
	
	add_theme_support('post-thumbnails');
	add_image_size('115x115', 115, 115, false);
	add_image_size('135x135', 135, 135, false);
	
	// custom sharethis shortcode 
	if (function_exists('st_makeEntries')){ add_shortcode('sharethis', 'st_makeEntries'); 
	}
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    
	 function cheapsideJsScripts() {
		wp_enqueue_script( 'js-file', get_template_directory_uri() . '/js/myscript.js');
	 }
	 add_action('wp_enqueue_scripts','cheapsideJsScripts');
    
    
    
    
    
    
    
    
    function showImageAttached( $_attachmentID, $width = "", $height = "", $class = "", $return = "echo", $size = "full" )
{
	if( hasImageAttached($_attachmentID) )
	{
		$attr = wp_get_attachment_image_src( $_attachmentID, $size );
		if($width != ""){ $width = ' width="'.$attr[1].'" '; }
		if($height != ""){ $height = ' height="'.$attr[2].'" '; }
		if($class != ""){ $class = ' class="'.$class.'" '; }
		$title = get_the_title($_attachmentID);
		$alt = get_post_meta( $_attachmentID, '_wp_attachment_image_alt', true);
		if($alt == ""){ $alt = $title; }
		
		if($return == 'echo')
		{
			echo '<img src="'.$attr[0].'" '.$width.' '.$height.' '.$class.' title="'.$title.'" alt="'.$alt.'" />';
		}
		elseif($return == 'return')
		{
			return 	'<img src="'.$attr[0].'" '.$width.' '.$height.' '.$class.' title="'.$title.'" alt="'.$alt.'" />';
		}
		else{}
	}
}

function showImageURL( $_attachmentID, $size = "full" )
{
	$attr = wp_get_attachment_image_src( $_attachmentID, $size );
	echo $attr[0];
}

function getImageURL( $_attachmentID, $size = "full" )
{
	$attr = wp_get_attachment_image_src( $_attachmentID, $size );
	return $attr[0];
}

function showImageALT( $_attachmentID, $return = 'echo' )
{
	$alt = get_post_meta( $_attachmentID, '_wp_attachment_image_alt', true);
	
	if($return == 'echo')
	{
		if($alt != ""){ echo $alt; }
	}
	elseif($return == 'return')
	{
		if($alt != ""){ return $alt; }
	}
	else{}
}

function hasImageAttached($_attachmentID)
{
	$attr = wp_get_attachment_image_src( $_attachmentID, 'full' );
	if($attr[0] != "")
	{
		return true;
	}
	else
	{
		return false;	
	}
}

function get_the_slug( $id=null ){
  if( empty($id) ):
    global $post;
    if( empty($post) )
      return '';
    $id = $post->ID;
  endif;

  $slug = basename( get_permalink($id) );
  return $slug;
}









	// Declare sidebar widget zone
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));
    }

    //Declare Navigation Menus
    if (function_exists('register_nav_menus')) {
    	register_nav_menus	(
    		array ('main_nav' => 'Main Navigation Menu'
    			)
    		);
    }

    if (function_exists('register_nav_menus')) {
    	register_nav_menus	(
    		array ('footer_nav' => 'Footer Navigation Menu'
    			)
    		);
    }

    if (function_exists('register_nav_menus')) {
    	register_nav_menus	(
    		array ('page_nav' => 'Page Navigation Menu'
    			)
    		);
    }
    //Declare New Customer Post
	add_action( 'init', 'create_post_type' );
function create_post_type()
{
	
	register_post_type( 'leisure-and-fitness',
		array(
			'labels' => array(
				'name' => __( 'Leisure and Fitness' ),
				'singular_name' => __( 'Leisure and Fitness' )
			),
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);
	
	register_post_type( 'theroyalexchange',
		array(
			'labels' => array(
				'name' => __( 'The Royal Exchange' ),
				'singular_name' => __( 'The Royal Exchange' )
			),
		'menu_position' => 19,
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug'=>'the-royal-exchange','with_front'=>false),
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);
	
	register_post_type( 'participating-brands',
		array(
			'labels' => array(
				'name' => __( 'Participating Brands' ),
				'singular_name' => __( 'Participating Brand' )
			),
		'menu_position' => 20,
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);

	register_post_type( 'retail-and-service',
		array(
			'labels' => array(
				'name' => __( 'Retail and Services' ),
				'singular_name' => __( 'Retail and Service' )
			),
		'menu_position' => 21,
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);
	register_post_type( 'food-and-drink',
		array(
			'labels' => array(
				'name' => __( 'Food and Drinks' ),
				'singular_name' => __( 'Food and Drink' )
			),
		'menu_position' => 23,
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);
	register_post_type( 'nightlife',
		array(
			'labels' => array(
				'name' => __( 'Nightlife and Entertainments' ),
				'singular_name' => __( 'Nightlife and Entertainment' )
			),
		'menu_position' => 24,
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'custom-fields', 'post-formats' )
		)
	);
	
	
	

}

	//remove version number from head & feeds
	function disable_version () {return '';}
	add_filter('the_generator','disable_version');
	remove_action('wp_head','wp_generator');








//Security Functions
function clear_format_rss($string)
{
	$string = strip_tags($string);
	$string = str_replace("'","'", $string);
	$string = str_replace('&#039;',"'",$string);
	$string = str_replace('"','"',$string);
	$string = str_replace('"','"',$string);
	$string = str_replace('&nbsp;',' ',$string);
	$string = stripslashes($string);
	return $string;
}

function sanitizeTextString($string)
{
	$string = strip_html_tags($string);
	$string = strip_tags($string);
	$string = htmlentities($string,ENT_QUOTES);
	return $string;
}

function formatHtml($string)
{
	$string = strip_tags($string);
	$string  = stripslashes($string);
	return $string;
}

function clean_data_escape_string($string)
{
	global $db_link;
	$string = strip_html_tags($string);
	$string = strip_tags($string);
	$string = htmlentities($string,ENT_QUOTES);
	return mysqli_real_escape_string($db_link, $string);
}

function escape_strings($string)
{
	global $db_link;
	$string = strip_html_tags($string);
	$string = strip_tags($string);
	return mysqli_real_escape_string($db_link, $string);
}

function remove_headers($string) { 
  $headers = array(
    "/to\:/i",
	"/To\:/i",
    "/from\:/i",
	"/From\:/i",
    "/bcc\:/i",
	"/Bcc\:/i",
    "/cc\:/i",
	"/Cc\:/i",
    "/Content\-Transfer\-Encoding\:/i",
    "/Content\-Type\:/i",
    "/Mime\-Version\:/i" 
  ); 
  return preg_replace($headers, '', $string); 
}

function valid_email($email)
{
	if(filter_var($email, FILTER_VALIDATE_EMAIL))
	{
		return true;
	}
	else
	{
		return false; 
	}
}



function strip_html_tags($text){
	$text = preg_replace(
		array("'<([%])[^>]*?.*?([%])>'si",
		      "'<([?])php[^>]*?.*?([?])>'si",
		      "'<head[^>]*?>.*?</head>'si",
			  "'<style[^>]*?>.*?</style>'si",
			  "'<script[^>]*?>.*?</script>'si",
			  "'<object[^>]*?>.*?</object>'si",
			  "'<embed[^>]*?>.*?</embed>'si",
			  "'<applet[^>]*?>.*?</applet>'si",
			  "'<noframes[^>]*?>.*?</noframes>'si",
			  "'<noscript[^>]*?>.*?</noscript>'si",
			  "'<noembed[^>]*?>.*?</noembed>'si",
			  "'<label[^>]*?>.*?</label>'si",
			  "'<table[^>]*?>.*?</table>'si",
			  "'<button[^>]*?>.*?</button>'si",
			  "'<input[^>]*?>.*?</input>'si",
			  "'<blockquote[^>]*?>.*?</blockquote>'si",
			  "'<iframe[^>]*?>.*?</iframe>'si",
			  "'<frame[^>]*?>.*?</frame>'si",
			  "'<frameset[^>]*?>.*?</frameset>'si",
			  "'<select[^>]*?>.*?</select>'si",
			  "'<option[^>]*?>.*?</option>'si",
			  "'<textarea[^>]*?>.*?</textarea>'si",
			  "'<fieldset[^>]*?>.*?</fieldset>'si",
			  "'<legend[^>]*?>.*?</legend>'si",
			  "'<form[^>]*?>.*?</form>'si",
			  "'<optgroup[^>]*?>.*?</optgroup>'si"
			  ), array(""), $text );
	return $text;
}



function bgcolor(){
global $i, $bgcolor, $wc;
if($i % 2 == 0){
$bgcolor = "#FFFFFF";
$wc = "";
}else{
$bgcolor = "#F5F5F5";
$wc = "_2";
}
return $bgcolor; 
return $wc;
}

function strip_ext($name){ 
	global $ext;
    $ext_temp = strrchr($name, '.'); 
	$length = strlen($ext_temp);
    $ext = substr($ext_temp, 1, $length); 
	return $ext;
}

function clear_format($to_format)
{
	$text  = str_replace("&","&amp;", $to_format);
	$text  = stripslashes($text);
	$text  = str_replace("\n",'<br />', $text);
	return $text;
}

function nwords($string, $length, $ellipsis = "...") 
{
   $words = explode(' ', $string); 
   if (count($words) > $length) 
       return implode(' ', array_slice($words, 0, $length)) . $ellipsis; 
   else 
       return $string; 
} 

function nchar($string, $length, $ellipsis = "...") 
{
	if(strlen($string) > $length)
	{
		return substr($string, 0, $length) . $ellipsis; 
	}
	else 
	{
		return $string; 
	}
}

function replaceUnicode($arg)
{
	$arg  = str_replace("'","&lsquo;",$arg);
	$arg  = str_replace("'","&rsquo;",$arg);
	$arg  = str_replace('"',"&ldquo;",$arg);
	$arg  = str_replace('"',"&rdquo;",$arg);
	return $arg;	
}

// Friendly URLs
	function strip_file_name_dash($arg)
	{
		$arg  = stripslashes(strtolower($arg));
		$arg  = str_replace("&#039;"," ",$arg);
		$arg  = str_replace("&amp;","and",$arg);
		$arg  = str_replace("&quot;"," ",$arg);
		$arg  = str_replace("&","and",$arg);
		$arg  = preg_replace('/\W/', ' ', $arg);
		$arg  = preg_replace('/\s\s+/', ' ', $arg);
		$arg  = trim($arg);
		$arg  = str_replace(" ","-",$arg);
		return $arg;
	}
	
	function strip_file_name_slash($arg)
	{
		$arg  = stripslashes(strtolower($arg));
		$arg  = str_replace("&#039;"," ",$arg);
		$arg  = str_replace("&amp;","and",$arg);
		$arg  = str_replace("&quot;"," ",$arg);
		$arg  = str_replace("&","and",$arg);
		$arg  = preg_replace('/\W/', ' ', $arg);
		$arg  = preg_replace('/\s\s+/', ' ', $arg);
		$arg  = trim($arg);
		$arg  = str_replace(" ","/",$arg);
		return $arg;
	}
//


function get_file_name($title,$prefix)
{
	if($prefix != "")
	{
		$filename = $prefix."-";
	}
	$filename .= strip_file_name_dash($title).".php";
	return $filename;
}

function friendly_url($title,$prefix)
{
	if($prefix != "")
	{
		$filename = $prefix."/";
	}
	$filename .= strip_file_name_dash($title);
	return $filename;
}




function dbSanitizeTextString($string)
{
	global $db_link;
	$string = strip_html_tags($string);
	$string = cleanItUp($string);
	$string = strip_tags($string);
	$string = htmlentities($string, ENT_QUOTES, 'UTF-8');
	return mysqli_real_escape_string($db_link, $string);
}

function cleanItUp($cmt,$wordlist=null,$character="*",$returnCount=false)
{
    if($wordlist==null)
    {
        $wordlist="nigga|nigger|niggers|sandnigger|sandniggers|sandniggas|sandnigga|honky|honkies|chink|chinks|gook|gooks|wetback|wetbacks|spick|spik|spicks|spiks|bitch|bitches|bitchy|bitching|cunt|cunts|twat|twats|fag|fags|faggot|faggots|faggit|faggits|ass|asses|asshole|assholes|shit|shits|shitty|shity|dick|dicks|pussy|pussies|pussys|fuck|fucks|fucker|fucka|fuckers|fuckas|fucking|fuckin|fucked|motherfucker|motherfuckers|motherfucking|motherfuckin|mothafucker|mothafucka|motherfucka|2g1c|2 girls 1 cup|acrotomophilia|anal|anilingus|anus|arsehole|ass|asshole|assmunch|auto erotic|autoerotic|babeland|baby batter|ball gag|ball gravy|ball kicking|ball licking|ball sack|ball sucking|bangbros|bareback|barely legal|barenaked|bastardo|bastinado|bbw|bdsm|beaver cleaver|beaver lips|bestiality|bi curious|big black|big breasts|big knockers|big tits|bimbos|birdlock|bitch|black cock|blonde action|blonde on blonde action|blow j|blow your l|blue waffle|blumpkin|bollocks|bondage|boner|boob|boobs|booty call|brown showers|brunette action|bukkake|bulldyke|bullet vibe|bung hole|bunghole|busty|butt|buttcheeks|butthole|camel toe|camgirl|camslut|camwhore|carpet muncher|carpetmuncher|chocolate rosebuds|circlejerk|cleveland steamer|clit|clitoris|clover clamps|clusterfuck|cock|cocks|coprolagnia|coprophilia|cornhole|cum|cumming|cunnilingus|cunt|darkie|date rape|daterape|deep throat|deepthroat|dick|dildo|dirty pillows|dirty sanchez|dog style|doggie style|doggiestyle|doggy style|doggystyle|dolcett|domination|dominatrix|dommes|donkey punch|double dong|double penetration|dp action|eat my ass|ecchi|ejaculation|erotic|erotism|escort|ethical slut|eunuch|faggot|fecal|felch|fellatio|feltch|female squirting|femdom|figging|fingering|fisting|foot fetish|footjob|frotting|fuck|fuck buttons|fudge packer|fudgepacker|futanari|g-spot|gang bang|gay sex|genitals|giant cock|girl on|girl on top|girls gone wild|goatcx|goatse|gokkun|golden shower|goo girl|goodpoop|goregasm|grope|group sex|guro|hand job|handjob|hard core|hardcore|hentai|homoerotic|honkey|hooker|hot chick|how to kill|how to murder|huge fat|humping|incest|intercourse|jack off|jail bait|jailbait|jerk off|jigaboo|jiggaboo|jiggerboo|jizz|juggs|kike|kinbaku|kinkster|kinky|knobbing|leather restraint|leather straight jacket|lemon party|lolita|lovemaking|make me come|male squirting|masturbate|menage a trois|milf|missionary position|motherfucker|mound of venus|mr hands|muff diver|muffdiving|nambla|nawashi|negro|neonazi|nig nog|nigga|nigger|nimphomania|nipple|nipples|nsfw images|nude|nudity|nympho|nymphomania|octopussy|omorashi|one cup two girls|one guy one jar|orgasm|orgy|paedophile|panties|panty|pedobear|pedophile|pegging|penis|phone sex|piece of shit|piss pig|pissing|pisspig|playboy|pleasure chest|pole smoker|ponyplay|poof|poop chute|poopchute|porn|porno|pornography|prince albert piercing|pthc|pubes|pussy|queef|raghead|raging boner|rape|raping|rapist|rectum|reverse cowgirl|rimjob|rimming|rosy palm|rosy palm and her 5 sisters|rusty trombone|s&m|sadism|scat|schlong|scissoring|semen|sex|sexo|sexy|shaved beaver|shaved pussy|shemale|shibari|shit|shota|shrimping|slanteye|slut|smut|snatch|snowballing|sodomize|sodomy|spic|spooge|spread legs|strap on|strapon|strappado|strip club|style doggy|suck|sucks|suicide girls|sultry women|swastika|swinger|tainted love|taste my|tea bagging|threesome|throating|tied up|tight white|tit|tits|titties|titty|tongue in a|topless|tosser|towelhead|tranny|tribadism|tub girl|tubgirl|tushy|twat|twink|twinkie|two girls one cup|undressing|upskirt|urethra play|urophilia|vagina|venus mound|vibrator|violet blue|violet wand|vorarephilia|voyeur|vulva|wank|wet dream|wetback|white power|women rapping|wrapping men|wrinkled starfish|xx|xxx|yaoi|yellow showers|yiffy|zoophilia";
    }
    $replace = 'preg_replace("/./","' .$character .'","\\1")';
    $comment = preg_replace("/\b($wordlist)\b/ie", $replace,$cmt,-1,$count);

    if($returnCount!=false)
    {
        return $count;
    }
    elseif($returnCount!=true)
    {
        return $comment;
    }
}
function cleanWords(&$item)
{
	$item = cleanItUp($item);
}


function notEmpty($field)
{
	if(isset($field) && $field != "")
	{
		return true;	
	}
	else
	{
		return false;
	}
}

function checkDuplicateEmail()
{
	global $db_link;
	if(notEmpty($_POST["_email"]) == true)
	{
		$query = "	SELECT 
						* 
					FROM 
						cms_activations 
					WHERE 
						_email = '".dbSanitizeTextString($_POST["_email"])."'";
		$result = mysqli_query($db_link, $query);
		if(mysqli_num_rows($result) > 0){ return false; }
	}
	return true;
}

function checkDuplicateCardNumber()
{
	global $db_link;
	if(notEmpty($_POST["_card_number"]) == true)
	{
		$query = "	SELECT 
						* 
					FROM 
						cms_activations 
					WHERE 
						_card_number = '".dbSanitizeTextString($_POST["_card_number"])."'";
		$result = mysqli_query($db_link, $query);
		if(mysqli_num_rows($result) > 0){ return false; }
	}
	return true;
}



function checkDuplicateEmailNonMembers()
{
	global $db_link;
	if(notEmpty($_POST["_email"]) == true)
	{
		$query = "	SELECT 
						* 
					FROM 
						cms_activations_non_members
					WHERE 
						_email = '".dbSanitizeTextString($_POST["_email"])."'";
		$result = mysqli_query($db_link, $query);
		if(mysqli_num_rows($result) > 0){ return false; }
	}
	return true;
}

function checkDuplicateCardNumberNonMembers()
{
	global $db_link;
	if(notEmpty($_POST["_card_number"]) == true)
	{
		$query = "	SELECT 
						* 
					FROM 
						cms_activations_non_members
					WHERE 
						_card_number = '".dbSanitizeTextString($_POST["_card_number"])."'";
		$result = mysqli_query($db_link, $query);
		if(mysqli_num_rows($result) > 0){ return false; }
	}
	return true;
}




function send_confirmation($type, $email, $name)
{
	if($type == 'activation')
	{
		$message = 'Dear '.$name.'<br /><br />
The Cheapside Business Alliance would like to welcome you to the Cheapside privilege card scheme.<br /><br />
Your Cheapside privilege card has now been activated and is ready for use.<br /><br />	 
Please visit www.cheapsideprivilegecard.co.uk to see the range of offers available at One New Change, Royal Exchange and across Cheapside.<br /><br />
Kind regards<br /><br />
The Cheapside Business Alliance';

	smtpmailer($email, 'no-reply@cheapsideprivilegecard.co.uk', 'Cheapside Business Alliance', 'Cheapside Privilege Card - Activation', $message);
	
	}
	
	if($type == 'request')
	{
		$message = 'Thank you for getting in touch with the Cheapside Business Alliance.<br /><br />
Our team have received your contact request and will be in touch soon.<br /><br />
Kind regards<br /><br />
Cheapside Business Alliance';

smtpmailer($email, 'no-reply@cheapsideprivilegecard.co.uk', 'Cheapside Business Alliance', 'Cheapside Privilege Card - Order', $message);

	}
	
	if($type == 'request-non-member')
	{
		$message = 'Dear '.$name.'<br /><br />
The Cheapside Business Alliance would like to welcome you to the Cheapside privilege card scheme.<br /><br />
As soon as we have confirmed receipt of payment, a card/s shall be despatched to the address you have given.<br /><br />	 
Please visit www.cheapsideprivilegecard.co.uk to see the range of offers available at One New Change, Royal Exchange and across Cheapside.<br /><br />
Kind regards<br /><br />
The Cheapside Business Alliance';

	smtpmailer($email, 'no-reply@cheapsideprivilegecard.co.uk', 'Cheapside Business Alliance', 'Cheapside Privilege Card - Non-Member Order', $message);
	}
	
	if($type == 'notify-admin-non-member')
	{
		$message = 'Dear Admin, <br /><br />'
		.$name.' has ordered a Non-Member Privilege Card. The payment should follow soon.';

	smtpmailer($email, 'no-reply@cheapsideprivilegecard.co.uk', 'Cheapside Business Alliance', 'Cheapside Privilege Card - Non-Member Order', $message);
	}
	
	if($type == 'notify-admin-member')
	{
		$message = 'Dear Admin, <br /><br />'
		.$name.' has ordered a Member Privilege Card.';

	smtpmailer($email, 'no-reply@cheapsideprivilegecard.co.uk', 'Cheapside Business Alliance', 'Cheapside Privilege Card - Member Order', $message);
	}
	

}

function smtpmailer($to, $from, $from_name, $subject, $body)
{
	$headers[] = 'Content-Type: text/html; charset=UTF-8';
	$headers[] = 'From: '.$from_name.' <'.$from.'>';
	$body = '<table cellpadding="0" cellspacing="0" border="0" width="600">
				<tr>
					<td style="padding: 20px; background-color: #ffffff;" bgcolor="#ffffff; text-align: center;" align="center">
					<img src="https://www.cheapsideprivilegecard.co.uk/wp-content/themes/cheapside/images/cheapside-logo.png" alt="Cheapside" style="display: block; margin: 0 auto;" />
					</td>
				</tr>
				<tr>
					<td style="font-family: Helvetica, Arial; font-size: 16px; line-height: 1.4em; color: #333; padding: 20px; background-color: #fff;" bgcolor="#fff">
					'.$body.'
					</td>
				</tr>
				<tr>
					<td style="font-family: Helvetica, Arial; font-size: 14px; line-height: 1.4em; color: #ffffff; padding: 20px; background-color: #00abbd; text-align: center;" align="center" bgcolor="#00abbd">
					Cheapside Privilege Card
					</td>
				</tr>
			</table>';
	wp_mail($to, $subject, $body, $headers);
}

/**
 * This function modifies the main WordPress query to include an array of 
 * post types instead of the default 'post' post type.
 *
 * @param object $query The main WordPress query.
 */
function tg_include_custom_post_types_in_search_results( $query ) {
	if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {
		 $query->set( 'post_type', array( 'post', 'retail-and-service', 'participating-brands', 'food-and-drink', 'nightlife', 'leisure-and-fitness' ) );
	}
}
add_action( 'pre_get_posts', 'tg_include_custom_post_types_in_search_results' );



function getCurlData($url)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_TIMEOUT, 10);
	curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
	$curlData = curl_exec($curl);
	curl_close($curl);
	return $curlData;
}
