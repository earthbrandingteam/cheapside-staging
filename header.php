<?php
global $db_link;
$db_link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($db_link, "cheap_live2013");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?v=<?php echo md5(date("Y-m-d-His")); ?>" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>
	<?php wp_head(); ?>
    
    <script type="text/javascript">
	<!--
	var switchTo5x=true;
	var __st_loadLate=true;
	-->
	</script>
	<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">
	<!--
	//wp.10778fd2-e2ea-4fc1-8736-d69cc8fa7bb1
	stLight.options({publisher: "69f23d5c-6377-4a6f-82c2-b20f28c650ff", doNotHash: true, doNotCopy: false, hashAddressBar: false});
	-->
	</script>
	
	<script src="<?php echo JS_DIR; ?>bigSlide.min.js"></script>
</head>
<body <?php body_class(); ?>>


<nav id="menu" class="panel" role="navigation">
	<div class="menu-container" style="padding: 54px 10px 10px 10px;">
	<?php wp_nav_menu(array('menu' => 'Mobile Navigation', 'menu_class' => 'menu-offers-navigation-menu')); ?>		
	<!-- <div class="shareThis socialMediaSharing">
		<p class="share-this-headline"><strong>Share this page</strong></p>
		<?php /* get_template_part('share-this'); */ ?>
	</div> -->
	</div>
</nav>
    
    
    
<div id="pageWrapper" class="clearfix">
	<div id="header">
		<div class="header-container">
			<div class="header-container-inner">
				<div>
					<a href="/" id="logo">Cheapside Privilege Card</a>
				</div>
				<div class="header-search-form">
					<div class="search-form">
						<?php get_search_form(); ?>
					</div>
					<a href="#menu" id="mobile-menu" class="menu-link">
						<span>&#9776;</span>
						<img id="close-btn" src="/wp-content/themes/cheapside/images/close-icon.svg" alt="Close icon">
					</a>	
				</div>
			</div>

		</div>
	</div>