<?php 
get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
			<div id="mainPage">
				<?php get_sidebar(); ?>				
				<div id="mainContent">
					<div id="pageTitle" class="halfheight">
						<h1 class="blue"><?php the_title(); ?></h1>
					</div>
					<div id="privacyAndAccessibilityContent">
						<div class="text black"><?php the_content(); ?></div>
					</div>
				</div> <!-- END MAINCONTENT -->
			</div> <!-- END MAINPAGE -->
<?php 
endwhile; endif;
get_footer(); ?>